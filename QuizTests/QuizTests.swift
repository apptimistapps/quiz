//
//  QuizTests.swift
//  QuizTests
//
//  Created by Dee Odus on 23/12/2021.
//

import XCTest
@testable import Quiz

class QuizTests: XCTestCase {

	var sut: QuizEngine!

    override func setUpWithError() throws {
        
		sut = QuizEngine(quiz: QuizAPIClient.createQuizQuestions())
    }

    override func tearDownWithError() throws {
        
		sut = nil
    }

	//MARK: Test that quiz questions are more than zero, if it fails then we don't have questions
	func test_quizHasQuestions(){
		
		XCTAssertGreaterThan(sut.quizCount, 0)
		
	}
	
	//MARK: Test that when quiz starts then score is 0 and progress is 0
	func test_quizStartsAtFirstQuestion_ScoreIsZeroProgressIsZero(){
		
		XCTAssertEqual(sut.score, 0)
		XCTAssertEqual(sut.progress, 0)
	}
	
	//MARK: Test that when correct selections are made they match the correct answers
	func testCorrectSelections_matchesCorrectAnswers(){
		
		XCTAssertEqual(sut.quiz.first!.correct, "Trump")
		XCTAssertEqual(sut.quiz.last!.correct, "Potato")
	}
	
	//MARK: Test that when getNextQuestion() function is called then the next question is returned
	func testGetNextQuestions_ReturnsNextQuestion(){
		
		XCTAssertEqual(sut.getNextQuestion().question, "Who's the 45th President of the USA?")
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "")

		XCTAssertEqual(sut.getNextQuestion().question, "Which sport does Roger Federer Plays?")
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "")
		
		XCTAssertEqual(sut.getNextQuestion().question, "What's the fastest land animal?")
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "")
		
		XCTAssertEqual(sut.getNextQuestion().question, "Which of these is not a fruit?")

	}
	
	//MARK: Test that when user answers first question incorrectly the score is 0 but progress is 1
	func test_AnswerFirstQuestionIncorrectly_ScoreIsZeroProgressIsOne(){
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Obama")
		
		XCTAssertEqual(sut.score, 0)
		XCTAssertEqual(sut.progress, 1)
	}
	
	//MARK: Test that when user answers first question correctly the score is 1 and progress is 1
	func test_AnswerFirstQuestionCorrectly_ScoreIsOneProgressIsOne(){
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Trump")
		
		XCTAssertEqual(sut.score, 1)
		XCTAssertEqual(sut.progress, 1)
	}
	
	//MARK: Test that when user answers the first two questions correctly the score is 2 and progress is 2
	func test_AnswerTwoQuestionsCorrectly_ScoreIsTwoProgressIsTwo(){
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Trump")
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Tennis")
		
		
		XCTAssertEqual(sut.score, 2)
		XCTAssertEqual(sut.progress, 2)
	}
	
	//MARK: Test that when user answers all four questions, two correctly, the score is 2 but progress is 4
	func test_AnswerFourQuestionTwoCorrectly_ScoreIsTwoProgressIsFour(){
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Trump")

		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Soccer")
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Cheetah")

		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Tomato")
		
		XCTAssertEqual(sut.score, 2)
		XCTAssertEqual(sut.progress, 4)
	}
	
	//MARK: Test that when user answers all four questions, all incorrectly, the score is 0 but progress is 4
	func test_AnswerFourQuestionAllincorrectly_ScoreIsZeroProgressIsFour(){
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Obama")

		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Soccer")
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Rabbit")

		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Tomato")
		
		XCTAssertEqual(sut.score, 0)
		XCTAssertEqual(sut.progress, 4)
	}

	//MARK: Test that when all questions are answered then the quiz end
	func test_AnswerAllQuestion_QuizEnd(){
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Obama")
		XCTAssertEqual(sut.quizEnded, false)

		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Soccer")
		XCTAssertEqual(sut.quizEnded, false)
		
		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Rabbit")
		XCTAssertEqual(sut.quizEnded, false)

		sut.answerQuestion(correct: sut.getNextQuestion().correct, selected: "Tomato")
		XCTAssertEqual(sut.quizEnded, true)
		
	}
}
