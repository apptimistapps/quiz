//
//  QuizAPIClient.swift
//  Quiz
//
//  Created by Dee Odus on 23/12/2021.
//

import Foundation

final class QuizAPIClient{
	
	//This will be replaced with URLSession to fetch data from the network
	static func createQuizQuestions() -> [Quiz]{
		
		[
			Quiz(question: "Who's the 45th President of the USA?", answer: ["Obama", "Trump", "Bush"], correct: "Trump"),
			
			Quiz(question: "Which sport does Roger Federer Plays?", answer: ["Tennis", "Golf", "Basketball", "Soccer"], correct: "Tennis"),
			
			Quiz(question: "What's the fastest land animal?", answer: ["Hare", "Rabbit", "Cheetah", "Antelope"], correct: "Cheetah"),
			
			Quiz(question: "Which of these is not a fruit?", answer: ["Strawberry", "Tomato", "Potato"], correct: "Potato")
		
		]
	}
}
