//
//  Quiz.swift
//  Quiz
//
//  Created by Dee Odus on 23/12/2021.
//

import Foundation

struct Quiz {
	
	let question: String
	let answer: [String]
	let correct: String
}
