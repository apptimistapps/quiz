//
//  QuizEngine.swift
//  Quiz
//
//  Created by Dee Odus on 23/12/2021.
//

import Foundation

final class QuizEngine{
	
	private(set) var progress = 0
	private(set) var score = 0
	let quiz: [Quiz]
	private(set) var quizEnded = false
	
	init(quiz: [Quiz]) {
		
		self.quiz = quiz
	}
	
	var quizCount: Int{
		
		return quiz.count
	}
	
	//MARK: get the next quiz based on progress, this is to make sure that when progress (which will be persisted) is provided we can always get the next question
	func getNextQuestion() -> Quiz{
		
		quiz[progress]
	}
	
	//MARK://Called when a question is answered, correct and selected answer sent and the progress is increased, if they both match then score is increased
	func answerQuestion(correct: String, selected: String){
		
		progress += 1
		if correct == selected{
			
			score += 1
		}
		
		if progress == quiz.count{
			
			endQuiz()
		}
	}
	
	//MARK: Set variable to true when all questions have been answered
	func endQuiz(){
		
		quizEnded = true
	}
	
	
}
